import re

file1 = open("wiki-100k.txt", "r", encoding="utf8")
lines = file1.readlines()
for i in lines:
    i = i.strip()
    if len(i) >= 5 and re.match("^[a-zA-Z]*$", i):
        print(i)